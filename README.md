# myNannyMVP

TODO: Write a project description
## System requirements

* Python 3.3+
* virtualenv (optional but strongly recommended)

## Installation

```
git clone git@gitlab.com:melizeche/nanny.git
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt
python manage.py migrate
```
## Usage

```
python manage.py runserver 0.0.0.0:8000
```
## URLS
* /  
* /admin/
* /list/
* /nannies/<nanny_id>

## More info

* usernamne: admin
* password: adminadmin

## TODO

Implement API RESTful

## License

MIT
