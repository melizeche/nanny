from django.contrib.gis import admin

from .models import Nanny

# Register your models here.
class NannyAdmin(admin.OSMGeoAdmin):
    default_lat = -57.579
    default_lon = -25.291
    default_zoom = 18
    #readonly_fields = ('Latitude','Longitude')


admin.site.register(Nanny, NannyAdmin)
admin.site.site_title = 'MyNanny Admin'
admin.site.site_header = 'MyNanny Admin'
