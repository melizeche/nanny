from django.http import HttpResponse
from django.shortcuts import render

from apps.nanny.models import Nanny
# Create your views here.


def home(request):
    return HttpResponse("HELLO!!!<br> <a href='/list/'>Nannies list </a>")


def nanny_list(request):
    all = Nanny.objects.all()
    return render(request, "list.html", {'nannies': all})


def nanny_profile(request, id):
    nanny = Nanny.objects.filter(id=id).first()
    print(nanny)
    return render(request, "profile.html", {'nanny': nanny})
