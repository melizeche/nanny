from django.contrib.gis.db import models
from django.contrib.auth.models import User
# Create your models here.


class Nanny(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    address = models.CharField(max_length=200, blank=True, null=True)
    location = models.PointField(blank=True, null=True)
    pic = models.ImageField(upload_to="imgs/%Y/%m/", blank=True, null=True)
    rating = models.DecimalField(
        max_digits=3, decimal_places=1, blank=True, null=True)
    user = models.OneToOneField(User, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Nannies"
